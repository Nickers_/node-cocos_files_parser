
import path from 'path';
import fs from 'fs';

export default class File {
    public directory: string = null;
    public dirent: fs.Dirent = null;
    public extension: string = null;
    public stats: fs.Stats = null;

    constructor(dir?: string, dirent?: fs.Dirent) {
        if (!dir || !dirent) return;

        this.directory = dir;
        this.dirent = dirent;
        this.extension = path.extname(this.dirent.name);
    }
}