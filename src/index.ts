import config from './config.json';
import FileParser from './FileParser';
import File from './File';
import { CocosFiles, CocosFile } from './CocosFile';
import CocosParser from './CocosParser';
import fs from 'fs';

const parser: FileParser = new FileParser();
const cocosParser: CocosParser = new CocosParser(config);

parser.checkDir(config.path)
    .then((files: File[]) => cocosParser.parse(files))
    .then((cocosFiles: CocosFiles) => {
        let result: File[] = [];
        let extensions: string[] = config.filter.images;
        let totalSize: number = 0;

        for (let uuid in cocosFiles) {
            let file: CocosFile = cocosFiles[uuid];

            // if (file.used !== 0 || !extensions.includes(file.ext)){//file.name === null) {
            //     delete cocosFiles[uuid];
            // }

            if (
                file.used !== 0 || 
                !extensions.includes(file.extension) || 
                result.includes(file) || 
                isInIgnoredFolder(file)
            ) continue;

            result.push(file);
            if (file.stats && file.stats.size) totalSize += file.stats.size;
        }
        
        let json = JSON.stringify({ unused: result }, null, '\t');

        fs.writeFile('unusedFiles.json', json, 'utf8', () => {
            // console.log(result);
            console.log(result.length);
            console.log(totalSize / 1024 / 1024);
        });

        fs.writeFile('allFiles.json', JSON.stringify(cocosFiles, null, '\t'), 'utf8', () => {});
    })
    .catch(console.error);

    function isInIgnoredFolder(file: CocosFile) {
        for (let folder of config.ignoreFolders) {
            if (file.directory.indexOf(folder) !== -1) return true;
        }

        return false;
    }