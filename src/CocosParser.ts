import fs from 'fs';
import File from "./File";
import { CocosFile, CocosFiles, MetaFile, MetaUUID, Accumulate } from './CocosFile';

export default class CocosParser {
    public files: CocosFiles = {};

    private _searchExtensions: string[] = [".fire", ".prefab", ".anim"];

    constructor({ search }: any) {
        if (search && search.extensions) this._searchExtensions = search.extensions;
    }

    public parse(files: File[]): Promise<CocosFiles> {
        return new Promise((resolve, reject) => { 
            this.checkEvery(files, this.checkMeta.bind(this))
                .then(() => this.checkEvery(files, this.checkText.bind(this)))
                .then(() => resolve(this.files))
                .catch(reject);
        });
    }

    private checkEvery(files: File[], checkFunct: (file: File) => Promise<void>): Promise<void[]> {
        let promises: Promise<void>[] = [];

        files.forEach(file => {
            promises.push(checkFunct(file));
        });

        return Promise.all(promises);
    }

    private checkMeta(file: File): Promise<void> {
        let metaPath: string = `${file.directory}\\${file.dirent.name}.meta`;

        return new Promise((resolve, reject) => {
            fs.access(metaPath, (err) => {
                if (err) {
                    console.error(err);
                    return resolve();
                }

                fs.readFile(metaPath, 'utf-8', (err, data) => {
                    if (err) return reject(err.message);

                    const metaFile: any = JSON.parse(data);

                    let meta: MetaUUID = this.findMetaUUID(metaFile);

                    this.storeUUIDs(meta, file);

                    return resolve();
                });
            });
        });
    }

    private findMetaUUID(metaFile: MetaFile): MetaUUID {
        let meta: MetaUUID = { uuid: null, uses: [] };

        if (metaFile.uuid) meta.uuid = metaFile.uuid;

        let accumulate: Accumulate = { uuids: [] };

        this.findUUIDsInJSON(metaFile, accumulate);

        accumulate.uuids.forEach(uuid => {
            if (!meta.uses.includes(uuid) && uuid !== meta.uuid) meta.uses.push(uuid);
        });

        return meta;
    }

    private storeUUIDs(meta: MetaUUID, file: File): void {
        let cocosFile: CocosFile = new CocosFile(meta.uuid, file);

        if (this.files[meta.uuid]) {
            this.files[meta.uuid].uses.forEach(uuid => {
                if (cocosFile.uuid! === uuid) cocosFile.uses.push(uuid);
            });
            cocosFile.used = this.files[meta.uuid].used;
        }

        meta.uses.forEach(uuid => {
            if (!this.files[uuid]) this.files[uuid] = new CocosFile(uuid);

            this.files[uuid].uses.push(meta.uuid);

            if (!cocosFile.used && this.files[uuid] && this.files[uuid].used) cocosFile.used = this.files[uuid].used;

            if (!cocosFile.uses.includes(uuid) && uuid !== meta.uuid) cocosFile.uses.push(uuid);
        });

        this.files[meta.uuid] = cocosFile;
    }

    private checkText(file: File): Promise<void> {
        return new Promise((resolve, reject) => {
            if (!this.isValidExtension(file.extension)) return resolve();
            
            const filePath: string = file.directory + '\\' + file.dirent.name;
            
            fs.readFile(filePath, 'utf-8', (err, data) => {
                if (err) return reject(err.message);

                let accumulate: Accumulate = { uuids: [] };

                this.findUUIDsInJSON(JSON.parse(data), accumulate);
                
                accumulate.uuids.forEach(uuid => {                
                    this.addUsed(uuid);
                });

                return resolve();
            });
        });
    }

    private findUUIDsInJSON(json: any, accumulate: Accumulate): string[] {
        let type: string = typeof(json);

        if (type === "string") {
            let uuid: string = json as string;

            if (this.validateUUID(uuid)) accumulate.uuids.push(uuid);
        }

        if (type !== "object" || json === null) return;

        if (Array.isArray(json)) {
            for (let value of json) {
                this.findUUIDsInJSON(value, accumulate);
            }

            return;
        }

        for (let field in json) {
            this.findUUIDsInJSON(json[field], accumulate);
        }
    };

    private validateUUID(str: string) {
        return /^([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})$/.test(str);
    }

    private isValidExtension(extension: string) {
        for (let validExtension of this._searchExtensions) {
            if (extension === validExtension) return true;
        }

        return false;
    }

    private addUsed(uuid: string): void {
        let accumulate: Accumulate = { uuids: [] };

        this.getAllUses(uuid, accumulate);

        accumulate.uuids.forEach(uuid => {
            if (this.files[uuid]) this.files[uuid].used++;
        });

        if (accumulate.uuids.length > 2) console.log(uuid, accumulate.uuids.length); 
    }

    private getAllUses(currentUUID: string, accumulate: Accumulate): void {
        if (!this.files[currentUUID]) this.files[currentUUID] = new CocosFile(currentUUID);

        let currentFile: CocosFile = this.files[currentUUID];

        if (!accumulate.uuids.includes(currentUUID)) accumulate.uuids.push(currentUUID);

        currentFile.uses.forEach(useUUID => {
            if (accumulate.uuids.includes(useUUID)) return;

            accumulate.uuids.push(useUUID);

            this.getAllUses(useUUID, accumulate);
        });
    }
} 