import File from "./File";

export type CocosFiles = {
    [uuid: string]: CocosFile
}

export class CocosFile extends File {
    public used: number = 0;
    public uses: string[] = [];
    public uuid: string = null;

    constructor(uuid?: string, file?: File) {
        super();
        if (file) Object.assign(this, file);
        this.uuid = uuid;
    }
}

export type MetaUUID = {
    uuid: string,
    uses: string[]
}

export type Accumulate = {
    uuids: string[]
}

export type MetaFile = {
    ver: string,
    uuid: string,
    rawTextureUuid?: string,

    textures?: string[],
    size?: {
        width: number,
        height: number
    },
    scale?: number,
    type?: string,
    subMetas: {
        [fileName: string]: any
    }
}