import fs from 'fs';
import File from './File';

export default class FileParser {
    public files: File[] = [];

    public checkDir(dir: string): Promise<File[]> {
        return new Promise((resolve, reject) => {
            this.readDir(dir)
                .then((dirents: fs.Dirent[]) => {
                    let promise: Promise<void | File[]> = null;
                    let promises: Promise<void | File[]>[] = [];

                    dirents.forEach(dirent => {
                        let file = new File(dir, dirent);

                        if (dirent.isDirectory()) {
                            // console.log(dir, dirent.name);
                            promise = this.checkDir(file.directory + '\\' + file.dirent.name);
                        } else {
                            if (file.extension === '.meta') return;

                            promise = this.getStats(file.directory, file.dirent.name).then((stats: fs.Stats) => {
                                file.stats = stats;
                                
                                this.files.push(file);
                            });
                        }

                        promises.push(promise);
                    });

                    return Promise.all(promises);
                })
                .then(() => resolve(this.files))
                .catch(reject);
        });
    } 

    private readDir(path: string): Promise<fs.Dirent[]> {
        return new Promise((resolve, reject) => {
            fs.readdir(path, { withFileTypes: true }, (err, files) => {
                if (err) return reject(err.message);

                return resolve(files);
            });
        });
    }

    private getStats(dir: string, fileName: string): Promise<fs.Stats> {
        let filePath = dir + '\\' + fileName;
        return new Promise((resolve, reject) => {
            fs.stat(filePath, (err, stats) => {
                if (err) return reject(err.message);

                return resolve(stats);
            });
        });
    }
}